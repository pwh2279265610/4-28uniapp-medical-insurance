const baseurl = 'http://localhost:8383'
function getToken(){
	let mytoken=uni.getStorageSync('token')
	return mytoken
}
function request(url, method, data) {
	console.log('请求的参数',data,url);
	return new Promise((resolve, reject) => {
		uni.request({
			url: baseurl + url,
			method,
			data,
			header:{token:getToken()},
			success: (ress) => {
				
				const res = ress.data
				console.log('res',res);
				if (res.code == 200) {
					resolve(res)
				} else if (res.code == 401) {
					// 没有权限就跳转到登录界面
					uni.showToast({
						title: '没有权限',
						icon: 'none',
						duration: 1000
					})
				} else if (res.code == 500) {
					uni.showToast({
						title: '服务器500',
						icon: 'none',
						duration: 1000
					})
					reject(res)
				} else if (res.code == 202) {
					uni.showToast({
						title: res.data.msg,
						icon: 'none',
						duration: 1000
					})
					reject(res)

				} else if (res.code == 400) {
					uni.showToast({
						title: '开发者某个字段或参数填写不对',
						icon: 'none',
						duration: 1000
					})
					reject(res)
				} else {
					/* uni.showToast({
						title: 'fail未知错误',
						icon: 'none',
						duration: 1000
					}) */
					reject(res)
				}

			},
			fail: (err) => {
				uni.showToast({
					title: 'fail未知错误',
					icon: 'none',
					duration: 1000
				})
				reject(err)
			}
		})
	})
}
// 接口
const RequestApi = {
	// 查询链上所有病历
	QueryBlockChain: () => request('QueryBlockChain', 'GET', {}),
	// 查询所有病人
	QueryAllUsers: () => request('QueryAllUsers', 'GET', {}),

	// 出院
	/* {
    "docAdd":"1",//医生地址
    "patAdd":"3519514831"//病人地址
	} */
	OutHospital: (data) => request('OutHospital', 'POST', data),
	/* 
	 根据病人地址查询病人
	 {
	     "address":"3519514833"
	 }
	 */
	QueryPat: (data) => request('QueryPat', 'POST', data),
	/* 
	 根据用户地址查询病历信息
	 {
	     "address":"3519514831"
	 }
	 */
	QueryAllEvi: (data) => request('QueryAllEvi', 'POST', data),
	/* 
	 根据用户地址修改用户描述信息
	 {
	     "address":"3519514831",
	     "message":"我hjhkhkj"
	 }
	 */
	WriteMessaage: (data) => request('WriteMessaage', 'POST', data),
	/* 
	 区块链注册
	 {
	     "address":"3519514833", //病人地址
	     "pirvateKey":"asd", //私钥（相当于密码，访问区块链）
	     "name":"黄日东", 
	     "age":"19", 
	     "sex":"男", 
	     "message":"我需要一份日本骨科病历" //描述信息
	 }
	 */
	TYQX: (data) => request('TYQX', 'POST', data),
	/* 
	 授权
	 {
	     "queriedPat":"3519514832",//需要被授权病人（用户）的地址
	     "patAdd":"3519514831",//当前病人（用户）的地址
	     "permission":"2"//权限级别
	 }
	 */
	SetXiEvi: (data) => request('SetXiEvi', 'POST', data),
	/* 
	 修改病历
	 {
	     "docAdd":"1",//医生地址
	     "patAdd":"3519514831",//病人地址
	     "medical":"盘尼西林 一盒",//用药记录
	     "advice":"多喝热水",//医嘱
	     "symptoms":"持续高烧不退",//症状
	     "temperature":"39.8",//体温
	     "hospital":"北京协和医院"//医院
	 }
	 */
	WrtieEviCode: (data) => request('WrtieEviCode', 'POST', data),
	/* 
	验证（病人和医生绑定） 
	 {
	     "docAdd":"1",//医生地址
	     "patAdd":"3519514831"//病人地址
	 }
	 */
	Verification: (data) => request('Verification', 'POST', data),
	/* 
	 医生查询他的病人
	 {
	     "docAdd":"1"//根据医生地址查询他的病人
	 }
	 */
	QueryPeople: (data) => request('QueryPeople', 'POST', data),
	/* 
	管理被授权病历病历
	 {
	     "queriedPatAdd":"3519514833",谁
	     "caseId":"3519514831"主人
	 }
	 */
	GXGL: (data) => request('GXGL', 'POST', data),
	/* 
	 我能共享的病历地址
	 {
	     "patAdd":"3519514832"
	 }
	 */
	QueryGL: (data) => request('QueryGL', 'POST', data),
	/* 
	被共享的地址
	 {
	     "patAdd":"3519514833"
	 }
	 */
	QueryGX: (data) => request('QueryGX', 'POST', data),
	/* 
	 
	 {
	     "pageNo":"0",//当前页号（必填）
	     "pageSize":"5"//每页大小（必填）
	     
	 }
	 */
	CategoryList:(data)=>request('api/product/list','POST',data),
	/* 
	 {
	     "productId":1,              //保险产品id
	     "rootId":1,                     //根评论id，如果是针对保险产品评论（即一级评论），则传入的rootId为-1
	                                     //如果是二级及以下评论，则rootId为跟评论id
	     "content":"这个保险还行吧",  //评论内容
	     "toCommentUserId":"1",       //所回复的目标评论的userId（user也就是客户customer）
	     "toCommentId":1             //所回复目标评论的id
	 }
	 
	 */
	CommentAddComment:(data)=>request('comment/addComment','POST',data),
	/* 
	{
	    "userName":"peng",
	    "password":"123456"
	} 
	 
	 */
	UserLogin:(data)=>request('user/login','POST',data),
	Compensate:(data)=>request('compensate','POST',data),
	CommentList:(id)=>{
		// console.log(id,'123');
		const url=`comment/commentList?productId=${id}&pageNo=1&pageSize=10`
		// console.log(url);
		// console.log('---',id);
		return request(url,"GET",{})
		},
	GetById:(id)=>{
		const url=`user/getById?id=${id}`
		return request(url,"GET",{})
	},
	BuyProduct:(data)=>request('buyProduct',"POST",data),
	Verification:(data)=>request('Verification','POST',data),
	// comment/addComment
	CommentAddComment:(data)=>request('comment/addComment','POST',data),
	// api/notice/list
	NoticeList:()=>request('api/notice/list','POST',{
		pageNo:21,
		pageSize:5
	}),
	// http://localhost:8383/  api/guarantee
	ApiGuarantee:()=>request('api/guarantee','POST',{
		
		    "effectivedate":"2023-04-27",//合同生效日
		    "insured":"xiaomao1",//被投保人姓名
		    "insuranceName":"康泰保险",//保险名称
		    "insuranceCategory":"小额医疗险",//保险种类名称
		    "insuredAge":"28",//被投保人年龄
		    "insuredSex":"0",////性别(0-男，1-女)
		    "currency":"人民币",//币种
		    "unit":"100",//币值单位
		    "basicAmount":"10000",//基本保险金额
		    "totalCost":"1000000",//合计十二个月保险费
		    "generalComp":"2000",//一般医疗补偿金
		    "substantialComp":"5000"//重大疾病医疗补偿金
		
	})
}


export {
	RequestApi
}
