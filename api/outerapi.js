const baseurl = 'https://meituan.thexxdd.cn/api/'

function request(url, method, data) {
	console.log('这是请求地址', data);
	return new Promise((resolve, reject) => {
		uni.request({
			url: baseurl + url,
			method,
			header:{Authorization:'Basic ZXlKaGJHY2lPaUpJVXpJMU5pSXNJblI1Y0NJNklrcFhWQ0o5LmV5SjFhV1FpT2lKdmJYQlRZVFIwV0ZWV2RFZGlWWFEyUTNSMFlWSmtWMG96YzJwTklpd2ljMk52Y0dVaU9qSXNJbWxoZENJNk1UWTRNekV5TkRnMk55d2laWGh3SWpveE5qZ3pNemcwTURZM2ZRLjQ1RVRHNktyM3dyeFpwQmVzZ0IxQW1aY0toVlROSnlDTENkWnZrWmNELWc6'},
			data,
			success: (res) => {
				console.log(res);
				if (res.statusCode == 200) {
					resolve(res)
				} else if (res.statusCode == 401) {
					// 没有权限就跳转到登录界面
					uni.showToast({
						title: '没有权限',
						icon: 'none',
						duration: 1000
					})
				} else if (res.statusCode == 500) {
					uni.showToast({
						title: '服务器发生位置错误',
						icon: 'none',
						duration: 1000
					})
					reject(res)
				} else if (res.statusCode == 202) {
					uni.showToast({
						title: res.data.msg,
						icon: 'none',
						duration: 1000
					})
					reject(res)

				} else if (res.statusCode == 400) {
					uni.showToast({
						title: '开发者某个字段或参数填写不对',
						icon: 'none',
						duration: 1000
					})
					reject(res)
				} else {
					uni.showToast({
						title: 'fail未知错误',
						icon: 'none',
						duration: 1000
					})
					reject(res)
				}

			},
			fail: (err) => {
				uni.showToast({
					title: 'fail未知错误',
					icon: 'none',
					duration: 1000
				})
				reject(err)
			}
		})
	})
}
// 接口
const RequestOuterApi = {
	// 获取视频资源
	VideoList: (data) => request('video_list', 'GET', data),
	FrontPage: () => request('frontpage', 'GET', {}),
	// 26. 获取抑郁症题目    GET    /depression_topics
	DepressionTopics: () => request('depression_topics', 'GET', {}),
	// 27. 抑郁症测试结果   GET   /depression?query=
	DepressiSon: (data) =>
		request('depression', 'GET', data),
	// 28. 获取早泄题目    GET   /premature_topics
	PrematureTopics: () => request('premature_topics', 'GET', {}),
	// 29. 早泄测试结果    GET   /premature?query=
	PremaTure: (data) => request('premature', 'GET', data),
	// 30. 获取失眠题目    GET   /insomnia_topics
	InsomniaTopics: () => request('insomnia_topics', 'GET', {}),
	// 31. 失眠测试结果    GET   /insomnia?query=
	InsoMnia: (data) => request('insomnia', 'GET', data),
}


export {
	RequestOuterApi
}
