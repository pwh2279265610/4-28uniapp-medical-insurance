const baseurl = 'http://localhost:8383'

function request(url, method, data) {
	console.log('reqlogin',data);
	return new Promise((resolve, reject) => {
		uni.request({
			url: baseurl + url,
			method,
			data,
			success: (ress) => {
				console.log('ress',ress);
				const res = ress.data
				if (res.code == 200) {
					resolve(res)
				} else if (res.code == 401) {
					// 没有权限就跳转到登录界面
					uni.showToast({
						title: '没有权限',
						icon: 'none',
						duration: 1000
					})
				} else if (res.code == 500) {
					uni.showToast({
						title: '服务器发生位置错误',
						icon: 'none',
						duration: 1000
					})
					reject(res)
				} else if (res.code == 202) {
					uni.showToast({
						title: res.data.msg,
						icon: 'none',
						duration: 1000
					})
					reject(res)

				} else if (res.code == 400) {
					uni.showToast({
						title: '开发者某个字段或参数填写不对',
						icon: 'none',
						duration: 1000
					})
					reject(res)
				} else {
					uni.showToast({
						title: 'fail未知错误',
						icon: 'none',
						duration: 1000
					})
					reject(res)
				}

			},
			fail: (err) => {
				uni.showToast({
					title: 'fail未知错误',
					icon: 'none',
					duration: 1000
				})
				reject(err)
			}
		})
	})
}
// 接口
const RequestLoginApi = {
	UserLogin: (data) => request('user/login', 'POST', data),
}


export {
	RequestLoginApi
}
