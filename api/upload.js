const uniUpLoadFile = (resimg) => {
	let promise;
	console.log('传入',resimg);
	promise = new Promise((resolve, rejct) => {
		uni.uploadFile({
			url: 'http://localhost:8383/upload', //服务器地址
			filePath: resimg,
			name: 'img', //我公司后端接口接收的参数是file(看你公司用什么参数名)
			success: res => {
				console.log('start');
				console.log(res);
				resolve(res.data)
			},
			fail: res => {
				uni.showToast({
					title: '上传图片失败'
				});
				console.log(res);
			}

		});
	});

	return promise;

}
export default uniUpLoadFile
