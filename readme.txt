安装:npm install 
运行:点击HBuilder X的运行->运行到小程序模拟器->微信开发者工具->在微信开发者工具配置Appid
api:封装的api接口
libs:外部库
node_modules:依赖库
pages:所有页面文件
static:静态文件资源
App.vue:入口文件
index.html:spa模板
main.js:程序入口文件
package.json:定义了这个项目所需要的各种模块，以及项目的配置信息（比如名称、版本、许可证等元数据）。
pages.json:pages.json文件和app.json文件的作用是一样的，但是呢，pages.json文件会覆盖掉app.json的配置。
		   而且，pages.json页面中配置项会覆盖 app.json 的 window 中相同的配置项，pages无需写 window 这个键
